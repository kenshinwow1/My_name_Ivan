package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Введите ваше имя:");

        Scanner input = new Scanner(System.in);
        String name = input.next();

        System.out.println("Добро пожаловать " + name);

    }

}
